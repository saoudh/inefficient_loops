/*
 * Created by Hussam Saoud on 20.2.17
 */
/**
 * Created by marija on 26.10.15.
 */
/*
 * Copyright 2014 Samsung Information Systems America, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// Author: Koushik Sen

// do not remove the following comment
// JALANGI DO NOT INSTRUMENT

var branches = {};
var loop_index = 0;
var durchlauf_index = 0;
var if_anweisung_index = 0;
var loop_begin = false
var if_begin = false
var if_condition_begin = false
var loops = [];
var durchlaeufe = [];
var if_anweisung = [];
var var_bedingung = [];
var var_body = [];
var for_body_var = []
var bedingung_result = null;
var for_condition_begin = false;;
var for_condition_var = [];
var for_condition_var_names = [];
var binary_ops = [];
var declaredVariables = [];
var bug_loop;
var var_body_functions = [];
var for_body_var_functions = [];
var relevant_for_body_var = [];
var relevant_var_conditions = [];
var aufruf_ifbody_index = 0;
var aufruf_ifbedingung_index = 0;
var aufruf_forbody_index = 0;
var isResultInstruction = false;
var currentScope;
var varScope;
var frames;


function calculate_result() {
  for (var i = 0; i < loops.length; i++) {
    var bug_if_anweisung = [];
    var bug_variables = [];
    var if_anweisung_index;
    var bug_if = true;
    var breakoutofloop = false;
    var relevant_var_conditions = [];

    for (var j = 0; j < loops[i].durchlaeufe.length; j++) {

      var relevant_for_body_var = loops[i].durchlaeufe[j].relevant_for_body_var;
      var x;
      for (x in relevant_for_body_var) {
        console.log("ResultInstruction-name=" + x + "-value=" +
          relevant_for_body_var[x].isResultInstruction);
        if (relevant_for_body_var[x].isResultInstruction) {
          breakoutofloop = true;
          break;
        }
      }
      if (breakoutofloop)
        break;

      for (var k = 0; k < loops[i].durchlaeufe[j].if_anweisung.length; k++) {
        if (breakoutofloop)
          break;

        var l;
        if_anweisung_index = k;
        for (l in loops[i].durchlaeufe[j].if_anweisung[k].relevant_var_conditions) {
          if (breakoutofloop)
            break;

          relevant_var_conditions = loops[i].durchlaeufe[j].if_anweisung[k]
            .relevant_var_conditions;

          if (typeof relevant_for_body_var[l] != 'undefined') {

            //If the result of the if-condition is true, then we cannot have a bug, as it would always be true
            if (loops[i].durchlaeufe[j].if_anweisung[k].bedingung_result) {

              //If the value is set in the for loop, then it means that there is no break-condition necessary
              if (relevant_var_conditions[l].val == relevant_for_body_var[l]
                .val) {
                bug_variables.forEach(function(element, index, array) {
                  if (array[index].name == l) {
                    array.splice(index, 1);
                    return;
                  }
                });
                bug_if = false;
                breakoutofloop = true;
                if (breakoutofloop)
                  break;
              } else {

                //The Value to be changed to should take the opposite value of the current
                bug_variables.forEach(function(element, index, array) {
                  if (array[index].name == l) {
                    array[index].val = relevant_var_conditions[l].val;
                    return;
                  }
                  if (array.length - 1 == index) {
                    array[array.length] = {
                      name: l,
                      val: relevant_var_conditions[l].val
                    };
                  }
                });


              }
            } else if (!loops[i].durchlaeufe[j].if_anweisung[k].bedingung_result) {

              if (relevant_var_conditions[l].val == relevant_for_body_var[l]
                .val) {


                //The Value to be changed to should take the opposite value of the current
                if (bug_variables.length > 1) {

                  bug_variables.forEach(function(element, index, array) {
                    if (array[index].name == l) {
                      array[index].val = relevant_var_conditions[l].val;
                      return;
                    }
                    if (array.length - 1 == index) {
                      array[array.length] = {
                        name: l,
                        val: relevant_var_conditions[l].val
                      };
                    }
                  });
                } else if (bug_variables.length == 0) {


                  bug_variables[bug_variables.length] = {
                    name: l,
                    val: relevant_var_conditions[l].val
                  };
                }


              } else {


                bug_if = false;
                break;
              }
            }



          } else {

            if (loops[i].durchlaeufe[j].if_anweisung[k].bedingung_result) {

              //The Value to be changed to should take the value of the current
              //If the bug-variable is already saved, then change the value appropriatly, otherwise add it to the array
              bug_variables.forEach(function(element, index, array) {
                if (array[index].name == l) {
                  array[index].val = !relevant_var_conditions[l].val;
                  return;
                }
                if (array.length - 1 == index) {
                  array[array.length] = {
                    name: l,
                    val: !relevant_var_conditions[l].val
                  };
                }
              });


            } else {


              if (bug_variables.length > 0) {
                bug_variables.forEach(function(element, index, array) {


                  if (array[index].name == l) {
                    array[index].val = relevant_var_conditions[l].val;
                    return;
                  }
                  if (array.length - 1 == index) {
                    array[array.length] = {
                      name: l,
                      val: relevant_var_conditions[l].val
                    };
                  }
                });
              } else if (bug_variables.length == 0) {

                bug_variables[bug_variables.length] = {
                  name: l,
                  val: relevant_var_conditions[l].val
                };
              }



            }
          }
        }
      }

    }


    //Only if there is a bug and we should have broken out of the loop, then we have a break-condition
    if (bug_if && !breakoutofloop) {
      var myoutput = "";
      bug_variables.forEach(function(element, index, array) {
        if (array[index].val) {
          myoutput += array[index].name + " && ";
        } else {
          myoutput += "!" + array[index].name + " && ";
        }
      });
      myoutput = myoutput.slice(0, -4);


      console.log("BUG IN Loop number " + (i + 1) +
        " - Break-Condition: if(" + myoutput + ")" +
        "{break;}-breakoutofloop=" + breakoutofloop);
    }


  }



}

function output_result() {

  console.log("loops-array-länge=" + loops.length);
  console.log("loops-array-länge=" + loops.length);
  var myArray = ['A', 'B', 'C'];
  var mySet = new Set(myArray);
  var hasB = mySet.has('B');
  calculate_result();


}

//The function myFunctionEnter is not used yet!
function myFunctionEnter(functionname, args) {


  //If-Body
  if (if_begin && !if_condition_begin && loop_begin) {

    var_body_functions[name] = {
      name: functionname,
      args: args
    };

  }
  //In der For-Schleife und außerhalb der if-Anweisung und nach der for-condtion
  if (loop_begin && !for_condition_begin && !if_begin && !
    if_condition_begin) {
    //falls eine Funktion in einem For-Body-Block aufgerufen wird, dann kann es keinen Bug haben,
    //da eine Funktion eine Result Instruction ist und die Schleife dauernd was produktives tut
    bug_for = false;
    for_body_var_functions[functionname] = {

      args: args
    };

  }
}


function read_write(name, val, lhs, read) {


  //When in the loop, then check if current var written to is in frame of the loop
  if (loop_begin == true) {
    varScope = frames.getFrame(name);



    //As long as the var-scope doesn't equal a higher scope than the current Scope, it is not a Result Instruction
    var scope = currentScope;
    isResultInstruction = false;

    while (typeof scope != 'undefined') {
      scope = frames.getParentFrame(scope);
      if (scope === varScope) {
        isResultInstruction = true;
      }
    }

  }

  //save only for-Condition-variables
  if (for_condition_begin) {



    var lesen = false,
      schreiben = false;


    if (read == true) {
      lesen = true;
    } else if (read == false) {
      schreiben = true;
    }
    if (typeof for_condition_var[name] == 'undefined') {

      for_condition_var[name] = {
        val: val,
        lesen: lesen,
        schreiben: schreiben,
        lhs: lhs,
        isResultInstruction: isResultInstruction
      };
    }

    if (for_condition_var_names.indexOf(name) == -1) {
      for_condition_var_names[for_condition_var_names.length] = name;
    }


  }
  //Nur If-Condition
  if (if_condition_begin) {

    aufruf_ifbedingung_index++;


    var lesen = false,
      schreiben = false;

    if (typeof var_bedingung[name] != 'undefined') {
      lesen = (typeof var_bedingung[name].lesen == 'undefined' ? false :
        var_bedingung[name].lesen);
      schreiben = (typeof var_bedingung[name].schreiben == 'undefined' ?
        false :
        var_bedingung[name].schreiben);

    }

    if (read == true) {
      lesen = true;
    } else if (read == false) {
      schreiben = true;
    }

    if (for_condition_var_names.indexOf(name) == -1) {

      relevant_var_conditions[name] = {
        val: val,
        lesen: lesen,
        schreiben: schreiben,
        lhs: lhs,
        bedingung_result,
        isResultInstruction: isResultInstruction

      }
    }



    var_bedingung[name] = {
      val: val,
      lesen: lesen,
      schreiben: schreiben,
      lhs: lhs,
      isResultInstruction: isResultInstruction
    };



  }
  //If-Body
  if (if_begin && !if_condition_begin && loop_begin) {
    aufruf_ifbody_index++;


    var lesen = false,
      schreiben = false;

    if (typeof var_body[name] != 'undefined') {
      lesen = (typeof var_body[name].lesen == 'undefined' ? false :
        var_body[name].lesen);
      schreiben = (typeof var_body[name].schreiben == 'undefined' ?
        false :
        var_body[name].schreiben);
    }

    if (read == true) {
      lesen = true;
    } else if (read == false) {
      schreiben = true;
    }

    var_body[name] = {
      val: val,
      lesen: lesen,
      schreiben: schreiben,
      lhs: lhs,
      isResultInstruction: isResultInstruction
    };



  }
  //In der For-Schleife und außerhalb der if-Anweisung und nach der for-condtion
  if (loop_begin && !for_condition_begin && !if_begin && !
    if_condition_begin) {

    //  for_body_var[name + aufruf_forbody_index] = {
    var lesen = false,
      schreiben = false;
    if (typeof for_body_var[name] != 'undefined') {
      lesen = (typeof for_body_var[name].lesen == 'undefined' ? false :
        for_body_var[name].lesen);
      schreiben = (typeof for_body_var[name].schreiben == 'undefined' ?
        false :
        for_body_var[name].schreiben);
    }

    if (read == true) {
      lesen = true;
    } else if (read == false) {
      schreiben = true;
    }
    var for_condition_var_set = new Set(for_condition_var);

    //Falls Variable im Schleifen-Blcok keine Schleifenvariable ist und auf die ein Wert geschrieben wird
    if ((for_condition_var_names.indexOf(name) == -1) && schreiben) {
      //console.log("readwrite-relevantforbody written-name=" + name);

      relevant_for_body_var[name] = {
          val: val,
          lesen: lesen,
          schreiben: schreiben,
          lhs: lhs,
          isResultInstruction: isResultInstruction
        }
        //  console.log("readwrite-typeof relevantvarconditions=" + typeof relevant_var_conditions[ name]);



      for_body_var[name] = {

        val: val,
        lesen: lesen,
        schreiben: schreiben,
        lhs: lhs,
        isResultInstruction: isResultInstruction
      };

    }
  }
}

(function(sandbox) {
  require('./Frames2.js');

  function MyAnalysis() {

    frames = new sandbox.Frames();
    sandbox.frames = frames;


    this.literal = function(iid, val, hasGetterSetter) {

      if (typeof val === 'function') {

        frames.defineFunction(val);
      }

      if (val == 'for_loop_begin') {
        console.log('for_loop_begin starts');
        for_condition_begin = true;
        loop_begin = true;
        loop = [];
      }


      if (val == 'if_begin') {
        console.log('if begin')
        if_begin = true;
        var_bedingung = [];
        var_body = [];
        if_anweisung = [];



      }

      if (val == 'if_ends') {
        if_begin = false;
        if_condition_begin = false;


        if_anweisung[
          if_anweisung.length] = {
          var_body: var_body,
          var_body_functions: var_body_functions,
          var_bedingung: var_bedingung,
          relevant_var_conditions: relevant_var_conditions,
          binary_ops: binary_ops,
          bedingung_result: bedingung_result
        };
        relevant_var_conditions = []


        bedingung_result = null;
        binary_ops = [];
      }

      if (val == 'if_condition_begin') {
        if_condition_begin = true;
        loop_begin = true;

      }
      if (val == 'if_condition_ends' || val == 'if_ends') {
        if_condition_begin = false;
      }



      if (val == 'for_round_begin') {
        currentScope = frames.getCurrentFrame();

        durchlauf_index++;
        loop_begin = true;
        for_condition_begin = false;
        if_anweisung_index = 0;
      }

      if (val == 'for_round_ends') {


        durchlaeufe[durchlaeufe.length] = {
          if_anweisung: if_anweisung,
          for_body_var: for_body_var,
          relevant_for_body_var: relevant_for_body_var,
          for_body_var_functions: for_body_var_functions,
          for_condition_var: for_condition_var,
          declaredVariables: declaredVariables
        };
        //At the end of the loop clear all data

        declaredVariables = [];
        for_body_var = [];
        for_body_var_functions = [];
        for_condition_begin = true;

      }
      if (val == 'for_loop_ends') {


        //Abspeichern der Daten für die aktuelle For-schleife
        loops[loops.length] = {
          bug_for: bug_for,
          durchlaeufe: durchlaeufe,
          for_condition_var_names: for_condition_var_names
        };
        for_condition_var_names = []
        durchlaeufe = [];
        for_condition_var_names = [];
        loop_index++;
        loop_begin = false;
        durchlauf_index = 0;
        if_anweisung_index = 0;

      }
    };

    this.endExecution = function() {
      output_result();
    };

    this.conditional = function(iid, result) {


      if (if_condition_begin) {

        bedingung_result = result;


      }
    };

    this.binary = function(iid, op, left, right, result, isOpAssign,
      isSwitchCaseComparison, isComputed) {


      binary_ops[binary_ops.length] = {
        op: op,
        left: left,
        right: right,
        result: result
      };

    };

    this.write = function(iid, name, val, lhs, isGlobal, isScriptLocal) {


      read_write(name, val, lhs, false);



    };

    this.read = function(iid, name, val, isGlobal, isScriptLocal) {
      read_write(name, val, null, true);
    };

    this.declare = function(iid, name, val, isArgument, argumentIndex,
      isCatchParam) {
      frames.initialize(name);
    };

    this.functionEnter = function(iid, f, dis, args) {
      frames.functionEnter(f);
      if (loop_begin) {
        myFunctionEnter(f, args);
        console.log("functionEnter: f.name=" + f.name);
        console.log("functionEnter: f.length=" + f.length);
        console.log("functionEnter: args.length=" + args.length);

        console.log("functionEnter-args[0]" + args[0]);
      }
    };

    this._return = function(iid, val) {
      //if there is a return in the loop, then the loop ends
      loop_begin = false;

    };


    this.functionExit = function(iid, returnVal, wrappedExceptionVal) {
      frames.functionReturn();

    };


    this.scriptEnter = function(iid, instrumentedFileName,
      originalFileName) {
      frames.scriptEnter();

    };

    this.scriptExit = function(iid, wrappedExceptionVal) {
      frames.scriptReturn();

    };


    this.instrumentCodePre = function(iid, code) {
      frames.evalBegin();

    };


    this.instrumentCode = function(iid, newCode, newAst) {
      frames.evalEnd();


    };

  }

  sandbox.analysis = new MyAnalysis();
})(J$);
