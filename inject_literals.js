/*
 * Created by Hussam Saoud on 20.2.17
 */

//The script should be executed with "node inject_literals.js [Code for inserting literals]"

var fs = require('fs');
var ast_utils = require('./index.js')
var esprima = require('esprima');
var estraverse = require('estraverse');
var escodegen = require('escodegen');
var filename = process.argv[2];

var file = fs.readFileSync(filename, 'utf8');
var ast = esprima.parse(file);
var ast2 = ast_utils.parse(file)
console.log(JSON.stringify(ast2, null, 2));
//traverse should be could as much as we have injections, in this case 3 times

ast_utils.traverse(ast2,
  function(node) {
    console.log("node.type=" + node.type);
    //console.log("for.-node=" + escodegen.generate(node));

    //Look for a For-Statement and insert the literals
    if (node.type === 'ForStatement') {
      console.log("for-node=" + escodegen.generate(node));
      var code = escodegen.generate(node);
      console.log("for-code=" + code);
      console.log("for-code with slice=" + code.slice(0,
        code.length - 1));
      ast_utils.injectCode(ast2, [node.range[0], node.range[1]],
        "\n'for_loop_begin';\n" + code.slice(0,
          code.length - 1) +
        "\n'for_round_ends';}" +
        "\n'for_loop_ends';\n")
    }


  }, 9, true);

ast_utils.traverse(ast2,
  function(node) {
    //console.log("node.type=" + node.type);

    //Look for a For-Statement and insert the literals
    if (node.type === 'ForStatement') {

      ast_utils.injectCode(ast2, [node.body.range[0], node.body
        .range[
          1] +
        1
      ], "\n{'for_round_begin';\n" + escodegen.generate(node.body).slice(
        1))
    }


  }, 9, true);



ast_utils.traverse(ast2,
  function(node) {
    console.log("node.type=" + node.type);

    //Look for an if-statement and insert the literals
    if (node.type === 'IfStatement') {

      ast_utils.injectCode(ast2, [node.range[0], node.range[1]],

        "\n'if_begin';\n'if_condition_begin';\n" + escodegen.generate(
          node) +
        "\n'if_ends';\n")
    }


  }, 9, true);

ast_utils.traverse(ast2,
  function(node) {


    //Look for an if-statement and insert the literal for the end of the if-condition
    if (node.type === 'IfStatement') {
      console.log("if-range:" + node.consequent.range);
      console.log("if-range:" + node.tokens);
      //  console.log("node.consequent=" + escodegen.generate(node.consequent).slice(  1));

      ast_utils.injectCode(ast2, [node.consequent.range[0], node.consequent
          .range[
            1]
        ],


        "\n{'if_condition_ends';\n" + escodegen.generate(node.consequent)
        .slice(
          1))

    }


  }, 9, true);

var content = escodegen.generate(ast2);

console.log(escodegen.generate(ast2));
fs.writeFileSync('./code.js',
  content, 'utf8');
